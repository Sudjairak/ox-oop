import java.util.Scanner;

public class Game {
	private Board board;
	private Player x;
	private Player o;
	static Scanner kb = new Scanner(System.in);

	public Game() {
		o = new Player("O");
		x = new Player("X");
		board = new Board(x, o);

	}

	public void play() {
		showWelcome();
		board.printBoard();
		board.check();
		choice();
	}

	private void showWelcome() {
		System.out.println("Welcome to Tic Tac Toe.\n");
	}
	
	private void showGoodbye() {
		System.out.println("Thank you for playing.");
	}
	private void choice() {
		System.out.println("Do you want to play again? <yes/no>:");
		String ch = kb.next();
		if(ch.equalsIgnoreCase("yes")) {
			board = new Board(x, o);
			play();
		}else if(ch.equalsIgnoreCase("no")) {
			getStat();
			showGoodbye();
		}
	}
	private void getStat() {
		System.out.println("Stat: ");
		System.out.println("   Player   " + "Win  " + " Draw  " + " Lose  ");
		System.out.println("     X       " + x.getWin() + "      " + x.getDraw() + "     " + x.getLose() + "  ");
		System.out.println("     O       " + o.getWin() + "      " + o.getDraw() + "     " + o.getLose() + "  ");

	}

}
